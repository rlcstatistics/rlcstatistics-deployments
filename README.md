www.rlcstatistics.com

# API FastAPI
uvicorn main:api

# UI Streamlit
streamlit run ./app/main.py

to install psycopg2, you need to run this command before :
sudo apt install libpq-dev and then
pip3 install psycopg2

# UI typescript
npm install
npm run build
npm run start

to generate a secure random secret key that will be used for JWT token.
openssl rand -hex 32

On my EC2, just 
sudo apt  install docker.io to install docker -y
sudo apt  install docker-compose -y

# to launch docker and build image
sudo service docker start

sudo docker ps -a
sudo docker images -a

docker exec <container-id> cat /data.txt
docker exec -it <container-id> bash

sudo docker rm -f <CONTAINER_ID>
sudo docker network ls
sudo docker network create myown-net

sudo docker rm apicontainer
cd api
sudo docker build -f Dockerfile -t myapi:latest .
sudo docker run -e JWT_SECRET_KEY=$JWT_SECRET_KEY -p 8000:8000 --net myown-net --name apicontainer myapi

sudo docker stop appcontainer
sudo docker rm appcontainer
sudo docker build -f Dockerfile -t myfront:latest .
sudo docker run -e VITE_CONTAINER_API_NAME=$VITE_CONTAINER_API_NAME -p 5173:5173 --net myown-net --name appcontainer21 myfront
sudo docker run -d -it -p 8501:8501 --net myown-net --name appcontainer myapp
sudo docker run -d -it -p 8501:8501 --net myown-net --name appcontainer registry.gitlab.com/besstial/rlstatproject:latest

sudo docker run -v db_data:/. --rm -it -w /. alpine cp .sql_app.db .

sudo docker-compose build
sudo docker-compose up -d
sudo docker-compose down

for docker certbot
sudo docker-compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ --dry-run -d api.rlcstatistics.com -d rlcstatistics.com
sudo docker compose run --rm certbot certonly --webroot --webroot-path /var/www/certbot/ -d api.rlcstatistics.com -d rlcstatistics.com

# dev local run
sudo service docker start
sudo docker-compose -f docker-compose-dev.yml up --build
sudo docker-compose -f docker-compose-dev.yml down --remove-orphans

# to deploy the app on prod or to cert the web app:
comment the server port 443 on nginx.conf
sudo docker-compose up -d
sudo docker-compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ --dry-run -d rlcstatistics.com
if succeed, do:
sudo docker-compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ -d rlcstatistics.com
uncomment the port 443
sudo docker-compose restart

# terraform

terraform init
terraform plan
terraform apply
terraform deploy ?


# Certificate service
sudo docker-compose run --rm  certbot certonly --webroot --webroot-path /var/www/certbot/ -d rlcstatistics.com --email adrien.lacaille@gmail.com --non-interactive --agree-tos

# NGINX
sudo systemctl enable nginx
sudo systemctl status nginx
sudo systemctl stop nginx
sudo systemctl start nginx
sudo systemctl reload nginx
sudo systemctl restart nginx


